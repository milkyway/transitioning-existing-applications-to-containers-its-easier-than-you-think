# Lab I - Introduction to CodeReady Workspaces
===============================

This lab introduces [CodeReady Workspaces](https://developers.redhat.com/products/codeready-workspaces/overview/) and demonstrates a deployment of a simple Tomcat web application.

CodeReady Workspaces is Red Hat's supported version of [Eclipse Che](https://www.eclipse.org/che/) which is a cloud hosted development environment.  All development is done through the web browser against a containerized development environment. Just because the environment is  hosted in the cloud, this does not mean that application development needs to be cloud native - we support both traditional and cloud native code development. Some notable highlights of the technology are:
* It has the concept of Stacks - think of this as a preconfigured template defined by your team, consisting of your tools, runtimes, and such.  These templates are used to spin up workspaces already configured with all the tooling required.  No manual installation and configuration required!
* Development portability becomes much easier as all the developer needs is a web browser. No more synching between multiple local environments (just don't tell your manager that when you're on vacation).
* You still have shell access to the environment, just as you would expect when working locally (we cover this in one of the labs).
* As development is done in the cloud, the Security and Operations teams do not have to worry about how to handle fine-grained access to the local workstations.  


## Table of Contents
1. [Log into CodeReady Workspaces](#log-into-codeready-workspaces)
1. [Create a Workspace](#create-a-workspace)
1. [Build and Deploy the Web Application](#build-and-deploy-the-web-application)
1. [Run the Tomcat Web Application](#run-the-tomcat-web-application)

## Log into CodeReady Workspaces
1. CodeReady Workspaces has been deployed into OpenShift and can be accessed here:
http://codeready-workspaces.apps.##GUID##.openshiftworkshop.com

1. When you access the tool for the first time, you will need to register, so click the 'Register' link to the right of the username / password field. Registration information is specific to this lab environment and is thrown away when the lab is decommissioned, so any username/password/email address combination will do.

1. Once you have logged in, you will see the home screen:  
![import](/images/lab01-che-homepage.png)

   > This screen is where you would access existing Workspaces or create new Workspaces with the specific technologies preinstalled.

## Create a Workspace
Development shops often require pre-configured environments consisting of a standardized set of technologies (Java, Tomcat, Spring Boot, etc), commands, and tools pre-installed. CodeReady Workspaces allows just this with the concept of a Factory and the creation of the Workspace and required tools is simply driven from a configuration file, typically written in JSON.  

For these labs, we use Factories to show how quickly you can go from an empty development environment to fully configured without having to manually install all the backing technology.  Remember the days of installing the right JDK, installing Eclipse, configuring Eclipse to use the *right* JDK, importing the Project...?

CodeReady Workspaces has a very easy way to build a Workspace from a Factory (configuration file) via a simple URL. Enter the URL:
http://codeready-workspaces.apps.##GUID##.openshiftworkshop.com/f?url=http://github.com/egetchel/summit-lab1

This is a REST command to instruct CodeReady to import the contents of the Git repository and execute the factory configuration file located in the root of the repository. What is happening under the covers is that a fully containerized environment is being spun up, where you will be able to not only develop, test, and deploy code, but also access the environment via a shell.  

For this Factory definition, the the following are built and configured for this lab:
   - A base environment via a Docker image with OpenJDK and Tomcat pre-installed
   - The Java project has been imported into the workspace
   - Maven Commands pre-configured

After a few minutes, you should land in your Workspace, which should look like the following:
![import](/images/lab01-workspace-home.png)

The default view does not give a lot of real estate to work with, so there are few panels that can be collapsed.  
![import](/images/lab01-workspace-minimize.png)

Once the panels have been collapsed, you will see the Project structure on the left-hand side.  Click on this to expand the contents and you will see a typical web application structure.  

If you want to look at the configuration file that was used to create your Worksapce, open the `.factory.json` file in the root of the project (sometimes it doesn't clone with the project - in that case the source can be found at https://raw.githubusercontent.com/egetchel/summit-lab1/master/.factory.json).

## Build and Deploy the Web Application
Cool, so now that we have our workspace up, let's show that we can deploy a traditional, standalone, Tomcat web application. The key takeaway here is that even though we're running in the cloud, we can still work with traditional applications.

Right-click on the root of the project, and in the pop-up menu, select:

`Commands -> Deploy(2) -> Build and Deploy Locally`

![import](/images/lab01-build-deploy.png)
A new tab is opened at the bottom of the screen and you will see the build progress.  After a minute or so, the build will complete.

So, what just happened? We just executed one of the pre-configured Maven commands that were installed as part of the Factory (you can see it in the `.factory.json` file).  This executed a Maven build and the resulting WAR file was placed in the Tomcat `/webapps/` directory.

Remember we mentioned you have shell access?  If you want to see things for yourself, you also have command-line access to your running machine.  Click the `Terminal` tab on the bottom panel, which is an active shell.  You can change to the actual Tomcat directory, which is `/opt/tomcat8` and poke around:

    [jboss@workspacetcijl7d2lrk5dmdp projects]$ cd /opt/tomcat8/webapps
    [jboss@workspacetcijl7d2lrk5dmdp webapps]$ ll
    total 15656
    -rw-rw-r--. 1   185 root     8888 Nov  3  2017  README.txt
    -rw-r--r--. 1 jboss root  4109402 Apr 10 19:41 ROOT.war

This certainly looks like any typical Tomcat deployment, which it is.
Typical Linux commands such as `top`, `curl`, and others are available as they are part of the base container image.

## Run the Tomcat Web Application
Now that the web application has been built and staged, let's go ahead and run it.  Right-click on the project, and select

`Commands -> Common(2) -> Start Tomcat`

This will open another tab and start Tomcat up.  Note that in the output a preview url to your running application is displayed.  You can click on this link and in a new browser tab, you will see you running web application.

   > **NOTE:** If you see a message that the application is not available, give it a few moments and try it again - it takes a few seconds for the Tomcat instance to deploy and spin up.

## Summary
We just demonstrated how to access CodeReady Workspaces and interact with typical stand-alone web application.  

In the upcoming labs, we will go through a few scenarios where we can automatically containerize and deploy this application.

[Go to lab 2](/labs/lab2.md)
