# Lab II - Using OpenShift to Containerize the Tomcat Application
-------------------------
In the [previous lab](/labs/lab1.md), we demonstrated importing and deploying a stand-alone Tomcat web application.  In this lab, we will show how OpenShift can automatically containerize the same application and deploy the resulting container image into OpenShift.  This lab emphasizes the Source 2 Image (S2I) process that can be leveraged to automatically containerize existing web applications into container images using a standardized build process and leveraging supported base images.

## Table of Contents
1. [Log into OpenShift](#log-into-openshift)
1. [Create a Project](#create-a-project)
1. [Project Deployment](#project-deployment)
1. [Project Cleanup](#project-cleanup)

## Log into OpenShift
OpenShift can be interacted with in numerous ways. This lab will utilize the web interface. Each student was assigned a unique user name. The password for all the accounts is `r3dh4t1!`.

To access the web interface, navigate to

     https://master.##GUID##.openshiftworkshop.com

![create](/images/lab02-openshift-home-page.png)

## Create a Project
A [Project](https://docs.openshift.com/enterprise/3.0/architecture/core_concepts/projects_and_users.html) in OpenShift is a way to group related assets. A [Pod](https://kubernetes.io/docs/concepts/workloads/pods/pod/) is a Kubernetes term that is a group of one or more co-located containers that share storage and network.

1. In the upper-right hand section of the screen, click `Create Project` to launch the wizard used to create a Project. On the resulting window, enter a project name and display name for the project.  Since project names must be unique, make sure you include your assigned user name. For example, if you were assigned `user1`, use a project name of `user1-tomcat`.  For the description, you can enter `Tomcat Web App`
![create](/images/lab02-create-project.png)  
After entering field values, click the `Create` button.

1. Now that the empty Project has been created, the upper-right area of the screen will show a list of all current projects in the 'My Projects' section (there is currently only one).  

1. Click the Project name to navigate into the Project you just created.  
![create](/images/openshift-empty-project.png)
You are now in an empty Project.  

1. Let's add the Tomcat web application we used in the previous lab. Clicking the "Browse Catalog" button will show the various technologies that OpenShift can containerize out of the box.  The catalog of available technologies that is displayed allows for filtering.  In this case, select "Languages"
![create](/images/openshift-select-language.png)
...and click the "Java" button to bring up the next screen.

1. Next, we can specify the type of Java application we want to containerize and deploy. You can see various default offerings from a simple Java main (OpenJDK), to a fully featured JEE Application Server (Red Hat JBoss EAP) as well as JWS (Tomcat) to name a few.  
![create](/images/openshift-select-base-image.png)
Select the `Red Hat JBoss Web Server 3.1 Apache Tomcat 8 (no https)` button.

1. This will bring up a wizard to configure the Tomcat container.  
![create](/images/openshift-create-tomcat-webapp-step-1.png)
Click the "Next" button

1. The next screen allows individual configurations specific to the type of image you are using. For example, if this image also had a database component, you would be able to supply a username and password pair here. We will container-ize the same application we used in the previous lab. Update the screen with the following changes
  - Git Repository URL: https://github.com/egetchel/summit-lab1.git
  - Git Reference : master
  - Context Directory : /  
![create](/images/lab02-create-tomcat-webapp-step-2.png)  
The remaining values can remain as-is  
Hit the Next button.  

1. Select `Do not bind at this time`
![create](/images/lab02-create-tomcat-webapp-step-3.png)
Hit the "Create" button which will show that the application has been created.

1. Click the "Continue to the project overview" link on the next page.
![create](/images/lab02-create-tomcat-webapp-step-4.png)

## Project Deployment
After a minute or so, you should see a screen that looks something like the following:
![create](/images/lab02-application-overview.png)  

1. Expand the Deployment Config by hitting the toggle:
![create](/images/lab02-application-overview-deployment-config.png)

1. In the Networking section, you can see the public URL that has been exposed for your Tomcat application.  Clicking it will bring up the Tomcat application.
![create](/images/lab02-application-overview-route.png)

1. Clicking the link (highlighted by the red arrow above) will open a new browser tab and navigate you to your running web application. *Note, the URL will be specific to your OpenShift environment.*

So, what just we just do?  Essentially, we have leveraged OpenShift's ability to build container images in a standardized, automated way.  This is called Source 2 Image.  It allows us to compile our application (or inject an already built application) from a repository and using a standardized base image (in this case, consisting of RHEL, JDK and Tomcat), and layer in our application to produce a new image. The image is then placed in the Container Registry.

![](/images/lab02-s2i-overview.png)

There is nothing proprietary about the resulting image - it is an OCI compliant image and can be run within OpenShift, or on any system that has Docker or PodMan installed.

So, essentially, we took the same web app and deployed it to both a standalone Tomcat instance in Lab 1, and a containerized instance in this lab.

## Project Cleanup

Since we will use this Project later in a later lab, there is no cleanup needed.  In the [next lab](/labs/lab3.md) we will demonstrate how we can interact with the OpenShift environment directly from CodeReady Workspaces.
