# Lab III - Interacting with OpenShift from CodeReady Workspaces

In the [previous lab](/labs/lab2.md), we demonstrated that we can leverage OpenShift's Source to Image (S2I) capabilities to build containers from existing application code directly in OpenShift.

This is just one of many ways in which developers can build and deploy containers.  For example, developers could:
  * Build a Docker container off the command line (`docker build...`)
  * Build an OCI compliant container off the command line (`buildah bud...`)
  * Use tooling directly within Eclipse/JBoss Developers Studio to build and deploy containers
  * Tie into a Jenkins pipeline, for example, Jenkins can produce the WAR file and we can then have OpenShift containerize it.

...and that's just a few options.  Essentially, every development shop does things that works the best for them.

In this lab, we will demonstrate yet another way in which a developer can create a containerized image of their application and leverage OpenShift to run the containers - this time directly from within CodeReady Workspaces.

## Table of Contents
1. [Fabric8](#fabric8)
1. [Update the Project's POM](#update-the-projects-pom)
1. [Log Into OpenShift](#log-into-openshift)
1. [Create a Project](#create-a-project)
1. [Containerize and Deploy your Web Application](#containerize-and-deploy-your-web-application)
1. [Examine the Application on Openshift](#examine-the-application-on-openshift)
1. [Give Me More Application Instances!](#give-me-more-application-instances)
1. [Project Cleanup](#cleanup)
1. [Summary](#summary)

## Fabric8
[Fabric8](https://maven.fabric8.io/) is a Maven plugin that can automate a lot of work when it comes to creating images and interacting with OpenShift.  It has numerous commands available, of which, we will be using the ability to automatically layer our web application on top of an existing image, resulting in a fully-functioning containerized web application.

## Update the Project's POM
Go back into the CodeReady Workspaces tab in your browser
1. Since we will be using Maven to build our container and deploy it into OpenShift, we need to add some things to our `pom.xml` file.
    1. Expand the `SampleWebApp` project.
    1. Open `pom.xml` by double-clicking on the file name.
    ![](/images/lab03-edit-pom.png)
    1. Find the section with the comment `<!-- Lab 3 - Add the profile here -->`
    1. Directly under that comment paste in
       ```xml
       <profiles>
         <profile>
           <id>openshift</id>
           <build>
             <plugins>
               <plugin>
                 <groupId>io.fabric8</groupId>
                 <artifactId>fabric8-maven-plugin</artifactId>
                 <version>${fabric8-maven-plugin.version}</version>
                 <executions>
                   <execution>
                     <id>fmp</id>
                     <phase>package</phase>
                     <goals>
                       <goal>resource</goal>
                       <goal>build</goal>
                     </goals>
                   </execution>
                 </executions>
               </plugin>
             </plugins>
           </build>
         </profile>
       </profiles>
       ```
       > This brings in the [Fabric8 Maven plugin](https://maven.fabric8.io/), which provides tight Kubernetes & OpenShift integration to Maven. It is the tool we will use for deploying our application to OpenShift.

Where is the Save button?  That's another interesting thing about CodeReady Workspaces. There isn't one. Files are automatically saved - similar to Google Docs and Google Sheets.

What we've done here is given the Fabric8 plugin the information it needs in order to create the image.

## Log into OpenShift
As the Maven plugin interacts with OpenShift in order to deploy the container, we need to log into OpenShift to establish an active session.  
  1. Access the Terminal
  Click the `Terminal` tab on the bottom tab pane on the screen
  ![](/images/lab03-terminal.png)  

  1. Using the login name given to you at the start of the lab (`user1, user2, user3...`), proceed to log into OpenShift via the command line.  Remember, the password is `r3dh4t1!`
  - `oc login https://master.##GUID##.openshiftworkshop.com`
     > Because we are using self-signed certificates in this lab environment you may get a message saying that the server uses a certificate signed by an unknown authority and whether or not you want to allow it. Enter `y` to allow it.

     > You should see a message saying `Login successful`

  ![](/images/lab03-login.png)

## Create a Project
Your CodeReady workspace is now attached to the running OpenShift instance. That means anything you do in CodeReady will reflect in OpenShift and vice versa.  Pretty cool!

  1. Let's go ahead a create a project in OpenShift via CodeReady. Remember, just like in the previous lab, the name of the project needs to be unique for all users, so use the username assigned to you as part of the project name.  From the terminal window, type:

  `oc new-project <your user name>-tomcat-codeready`

  So, if my user name was `user1`, the command would look like `oc new-project user1-tomcat-codeready`

  1. You now have an empty Project in OpenShift.  If you go back to your OpenShift tab, you should see your new project in the OpenShift console.

  ![](/images/lab03-new-project-in-openshift-console.png)

## Containerize and Deploy your Web Application
Now we will leverage the Maven plugin to automatically build, containerize, and deploy our web application into OpenShift.

  1. On the root of the project, right click and select `Commands > Deploy (2) > Build and Deploy to OpenShift`

  ![](/images/lab03-build-and-deploy.png)

  In the command window, you will see Maven building the application, adding a layer to an existing Tomcat image with the compiled web application, and deploying the container to OpenShift.

## Examine the Application on Openshift
Once the build has completed, the Maven plugin will then instruct OpenShift to deploy the container image.  We can watch this happening right in OpenShift.

  1. Click on the Project in the OpenShift web console

  ![](/images/lab03-new-project-in-openshift-console.png)

  1. Depending on how quickly you navigated over, you may catch OpenShift in the process of spinning up the Pod, or it could already be deployed.  Once the Pod is up and running, your screen should look like the following:

  ![](/images/lab03-deployed-to-openshift.png)

  Once the number of Pods reads `1` and the circle goes from a light blue to a dark blue, your containerized Tomcat application will be accessible.  Simply click on the URL and you will be able to view your application.

  1. You can look at details of your application by expanding the caret on the left-hand side
  ![](/images/lab03-application-details.png)

## Give Me More Application Instances!
Let's highlight some of the values of containers.  We've all been faced with the situation where we need more application instances due to capacity demands.  With traditional infrastructure, this would usually mean provisioning a new machine, installing the operating system, patching the operating systen, installing any required software, deploying your application, and updating the load balancers to include the new server in the pool.  Hopefully that's automated, but still, there are a lot of steps and groups involved.

Want more application instances in OpenShift?  See that blue circle with the up and down arrows?  Go ahead and click the up arrow a few times.    

![](/images/lab03-scaling.png)

So, just happened?  We've instructed OpenShift to create new running instances from our image and automatically load balance traffic between all the running instances.  

Yes, it's that simple.

## Cleanup
Since we don't need this project anymore, we can instruct OpenShift to delete it.
![](/images/lab03-delete-project.png)
1. Click 'OpenShift' in the top-left corner
1. Next to the `userxx-tomcat-codeready` Project name, click the three dots to bring up the menu
1. Click 'Delete Project'
1. Enter the project name and click Delete

   > **NOTE:** Don't delete the `Tomcat Web App` project that we created in [Lab 2](/labs/lab2.md). We will re-use that project in the next lab.

## Summary
We've just demonstrated complete round-tripping of a standard web application between traditional and containerized deploments of the same base application code. In the [next lab](/labs/lab4.md) we will show how Jenkins can be leveraged to manage the building and testing of the containerized application in OpenShift.
