# Spring Boot Lab
In this lab we will take a Spring Boot application that uses a PostgreSQL database backend and show how to containerize it and run it on OpenShift.

The source for this lab is an adaptation of the well-known [Spring Music](https://github.com/cloudfoundry-samples/spring-music) reference application. The source has been forked and tailored a bit for this lab, mostly to bring it up-to-date with the most recent versions of things, as well as to simplify the implementation a little bit.

We'll first run the application locally and inspect it a little bit so that we become familiar with it. We'll then deploy it to OpenShift and inspect it while its running there. In the last portion of this lab we'll demonstrate how to tweak runtime configuration on OpenShift so that it picks up alternate configuration at runtime.

## Table of Contents
1. [Lab Setup](#lab-setup)
    1. [Close Existing Workspaces](#close-existing-workspaces)
    1. [Load New Workspace](#load-new-workspace)
1. [Run application locally](#run-application-locally)
    1. [Inspect Spring Boot Actuators](#inspect-spring-boot-actuators)
    1. [Inspect Swagger UI endpoint](#inspect-swagger-ui-endpoint)
1. [Setup OpenShift Project](#setup-openshift-project)
1. [Deploy Application to OpenShift](#deploy-application-to-openshift)
1. [Enhance Application](#enhance-application)
1. [Change Application Data Source](#change-application-data-source)
1. [Cleanup](#cleanup)
1. [Solution](#solution)

## Lab Setup
In this section we need to do some general housekeeping to be able to get our environment set up for working on the lab.

In your browser go to the CodeReady Workspaces (`http://codeready-workspaces.apps.##GUID##.openshiftworkshop.com`)

### Close Existing Workspaces
1. You may need to expand the CodeReady navigation bar to view on the left to view your workspaces. You can do this by clicking the arrow in the top-left corner.
2.
   ![expand-nav](../images/spring-boot-lab/expand-codeready-nav.png)

1. In the CodeReady Workspaces navigation bar on the left under the `RECENT WORKSPACES` heading, right-click each workspace and click `Stop`   
2.
   ![close-workspaces](../images/spring-boot-lab/close-workspaces.png)

### Load New Workspace
1. In your browser enter the URL http://codeready-workspaces.apps.##GUID##.openshiftworkshop.com/f?url=https://github.com/edeandrea/summit-lab-spring-music

   > This will create a new workspace while pulling in the source code for the lab. It will also do some pre-configuration of the project.

1. The import of the project may take a couple of minutes for all of the dependencies to download
    - While its getting set up, feel free to take a look at the application's source code if you'd like
    - You'll know it's complete when you see something like `[INFO ] [j.l.JavaLanguageServerLauncher 123]  - Started: Ready` in the `dev-machine` console at the bottom of the workspace
1. When it is complete you should see the `summit-lab-spring-music (master)` project show up in the navigation on the left-hand side

## Run Application Locally
The first thing we want to do is to run the application locally so that we become familiar with it and what it does.
1. In the project workspace navigation right-click the `summit-lab-spring-music (master)` project & choose `Commands -> Run (2) -> Run application locally`
    ![run-application-locally](../images/spring-boot-lab/run-application-locally.png)

   > This will build the application and run it locally. You'll notice at the top of the `Run application locally` window at the bottom of the workspace there is a `preview` url. The `preview` URL will be the URL to the application running within the CodeReady Workspace container. When you see the console looking something like the following that means that the application has started up.

    ![application-started-up](../images/spring-boot-lab/application-started-up.png)

1. Click on the `preview` URL. You should see the main UI of the application. Play around with it a little bit.
    - Change the way the main list is viewed
    - Change the sort of the list
    - Add a new album
    - Delete an album
    - Edit an album

   > The application is a simple Angular UI with a Spring Boot RESTful back-end using [Spring Data JPA](https://spring.io/projects/spring-data-jpa) as the persistence layer. The purpose of the application is to manage music albums. The list of albums is pre-populated at application startup. By default by not specifying any data source, Spring Boot automatically provides an embedded H2 database, which is what you're seeing here.

### Inspect Spring Boot Actuators
There are many different [Spring Boot Actuators](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready) that provide capabilities out-of-the-box for Spring Boot applications. Let's take a look at a few of them.
1. In the browser tab which shows the application UI, add `/actuator` to the end of the URL in the browser.
    - This is the "Actuator" actuator, which shows which actuators are currently enabled and their corresponding URLs
1. Click on the URL for the `health` actuator. You should see something like this
   > **NOTE:** This has been formatted for demonstration purposes here. What you are seeing in your browser may just be a single line of JSON.

   ```js
   {
     "status": "UP",
     "details": {
       "db": {
         "status": "UP",
         "details": {
           "database": "H2",
           "hello": 1
         }
       },
       "diskSpace": {
         "status": "UP",
         "details": {
           "total": 211239829504,
           "free": 197749899264,
           "threshold": 10485760
         }
       }
     }
   }
   ```

   > This shows the current status of the application is **UP**. It also shows that the database being used is an H2 database and it is also **UP**.

1. Go back to the actuator list and click on the `info` actuator. You should see a bunch of information related to the git repository/last commit as well as to the Maven build information
   > This information was generated at build time using the Maven [`git-commit-id-plugin`](https://github.com/git-commit-id/maven-git-commit-id-plugin). You can go back to the project's `pom.xml` file and see it's configuration there.

1. Go back to the actuator list and click on the `env` actuator
   > You should see a listing that shows every variable available in the environment, regardless of where the variable came from (i.e. System environment, JVM property, etc).

1. The `env` actuator also supports looking up a single property. In the browser's URL bar append `spring.datasource.url` to the URL (the full path in the URL bar should be `/actuator/env/spring.datasource.url`).
   > You'll notice here that there is nothing defined. When this happens and Spring Boot sees in your application that you require a `DataSource`, then Spring Boot autoconfiguration will automatically create an embedded H2 `DataSource` for you.

1. Go back to the actuator list and inspect any of the other actuator endpoints you would like. The [Spring Boot Actuators documentation](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready) has a good description of the other out-of-the-box actuators and what information they provide.

### Inspect Swagger UI Endpoint
The application also embeds the [Swagger UI](https://swagger.io/tools/swagger-ui/) endpoint into the application. The application code is documented using the Swagger/OpenAPI annotations and is configured with the [Springfox library](http://springfox.github.io/springfox/docs/current/), which is a library which knows how to generate OpenAPI documents from the Swagger/OpenAPI annotations. It can also deduce a lot of the information directly from Spring MVC, so it makes documenting your API much easier within Spring MVC.

In the source tree, the `com.redhat.summit2019.springmusic.api.AlbumController` class is the main Spring MVC controller used for the RESTful endpoints. The class `com.redhat.summit2019.springmusic.config.SwaggerConfig` is the class used for configuring the Springfox library.

1. In your browser go back to the main application UI and append the path `/swagger-ui.html` into the browser. You should see the following (the URLs will be different for you)
    ![swagger-ui](../images/spring-boot-lab/swagger-ui.png)
1. Expand the `album-controller`. This is the main controller that contains the endpoints used by the Angular UI of the application.
1. Expand the `/albums Get all albums` endpoint
   > This shows a description of the endpoint

1. Click the `Try it out` button
1. Click the `Execute` button.
   > This will perform a live `curl` to the application while showing you all the details of the request and response

   ![get-all-albums-response](../images/spring-boot-lab/swagger-ui-get-all-albums-response.png)
1. At the top of the swagger-ui page there is a link that ends with `/v2/api-docs`. Click this link.
   > This will show you the OpenApi contract of the application in JSON format

1. Feel free to play around with the other endpoints and see if you can invoke some of the other operations
   > The `error-controller` endpoint is an endpoint which can simulate various error conditions happening within the application

Once you are done playing around with the Swagger UI endpoint you can terminate the running application
1. Go back to the CodeReady Workspaces tab in your browser
1. At the bottom of the workspace there is a tab labelled `Run application locally` that has a small x to it's right. Click that.

    ![stop-local-application](../images/spring-boot-lab/stop-local-application.png)
1. A dialog will pop up asking if you want to continue terminating the application. Click `OK`.

## Setup OpenShift Project
In this section we will setup a project on OpenShift to hold our application. For this you will need the user id you were given at the beginning of the session.

1. In CodeReady Workspaces at the bottom of the workspace click on the `Terminal` tab
1. cd into the project
    - `cd summit-lab-spring-music`
1. Set the `OCP_USERNAME` environment variable according to the user id you were given at the beginning of the session
    - `export OCP_USERNAME=<USER_ID>` - **Replace `<USER_ID>` with the user id you were given at the beginning of the session**
1. Log into the OpenShift cluster
    - `oc login https://master.##GUID##.openshiftworkshop.com -u $OCP_USERNAME -p r3dh4t1!`
       > Because we are using self-signed certificates in this lab environment you may get a message saying that the server uses a certificate signed by an unknown authority and whether or not you want to allow it. Enter `y` to allow it.

       > You should see a message saying `Login successful`

1. Create a new project to host your application
    - `oc new-project $OCP_USERNAME-spring-music`
       > It's very important to make sure the project name has the `$OCP_USERNAME` prefix. All projects in OpenShift have to have unique names within a cluster. Since all users in the workshop are on the same cluster we need to make sure the project names are unique.

1. Go to [OpenShift's web interface](https://master.##GUID##.openshiftworkshop.com) and log in using your user credentials (username = your user id, password = r3dh4t1!). Once logged in, in the top-right corner of the dashboard you should see your project. Clicking on it will take you into the project, which right now should be empty.

## Deploy application to OpenShift
The first thing we're going to do is simply deploy the application, as-is, onto OpenShift.

1. Go back into the CodeReady Workspaces tab in your browser
1. We need to add some things to our `pom.xml` file to allow Maven to do most of the heavy-lifting for us
    1. Open `pom.xml` & find the section with the comment `<!-- Add the profile here -->`
    1. Directly under that comment paste in
       ```xml
       <profiles>
         <profile>
           <id>openshift</id>
           <build>
             <plugins>
               <plugin>
                 <groupId>io.fabric8</groupId>
                 <artifactId>fabric8-maven-plugin</artifactId>
                 <version>${fabric8-maven-plugin.version}</version>
                 <executions>
                   <execution>
                     <id>fmp</id>
                     <phase>package</phase>
                     <goals>
                       <goal>resource</goal>
                       <goal>build</goal>
                     </goals>
                   </execution>
                 </executions>
               </plugin>
             </plugins>
           </build>
         </profile>
       </profiles>
       ```
       > This brings in the [Fabric8 Maven plugin](https://maven.fabric8.io/), which provides tight Kubernetes & OpenShift integration to Maven. It is the tool we will use for deploying our application to OpenShift.

1. In the project workspace navigation right-click the `summit-lab-spring-music (master)` project & choose `Commands -> Deploy (1) -> Deploy to Openshift`

    ![deploy-to-openshift](../images/spring-boot-lab/deploy-to-openshift.png)
   > The deployment may take a few minutes to run, especially since this is the first time running. Maven needs to download all of it's dependencies. When its done you should see a message that looks like

      ```
      [INFO] ------------------------------------------------------------------------
      [INFO] BUILD SUCCESS
      [INFO] ------------------------------------------------------------------------
      [INFO] Total time: 04:52 min
      [INFO] Finished at: 2019-04-08T16:32:09Z
      [INFO] Final Memory: 54M/68M
      [INFO] ------------------------------------------------------------------------
      ```

1. Once completed, go back to the browser tab that you logged into the OpenShift cluster with
   > You should see your application with 1 running pod (note that the URL that shows will be different for you)

    ![initial-deployment](../images/spring-boot-lab/initial-deployment.png)

1. Click around in the OpenShift console and take a look at what was created for you
    - Deployment config
        1. From the left-hand navigation select `Applications -> Deployments` and then click on the `summit-lab-spring-music` deployment
            ![deployment-list](../images/spring-boot-lab/deployment-list.png)
        1. Click the `Configuration` tab
            - Single instance of the application
            - A [rolling deployment strategy](https://docs.openshift.com/container-platform/3.11/dev_guide/deployments/deployment_strategies.html#rolling-strategy)
            - Pre-configured [readiness & liveness probes](https://docs.openshift.com/container-platform/3.11/dev_guide/application_health.html)
                - The [Spring Boot Health actuator](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#production-ready-health) is used for both probes
            - [Triggers](https://docs.openshift.com/container-platform/3.11/dev_guide/builds/triggering_builds.html) for new deployments
                - Change of configuration
                - Change of image in build configuration

            ![deploymentconfig](../images/spring-boot-lab/deploymentconfig.png)

        1. Click the link labelled `summit-lab-spring-music-s2i` in the `Containers` section (or in the left-hand navigation select `Builds -> Builds`, then click on the `summit-lab-spring-music-s2i` build)
        1. Click the `Configuration` tab
        ![s2i-build](../images/spring-boot-lab/s2i-build.png)
            - The [build config](https://docs.openshift.com/container-platform/3.11/dev_guide/builds/index.html) is configured using a Source-to-Image (S2I) build which translates the application source code to an output image
            - The deployment config watches for changes to the image - when the image changes a new deployment is automatically triggered
1. Click on the `Overview` tab on the left-hand navigation & then click the URL that is next to the `summit-lab-spring-music` application
   > You should see the application come up and behave just like it did when you were running it locally. If you go to the `/actuator/health` endpoint like you did before you should still see that it is using an embedded H2 database.

## Enhance Application
Let's add some functionality to our application using the [spring-cloud-kubernetes](https://cloud.spring.io/spring-cloud-kubernetes/spring-cloud-kubernetes.html) project. This project allows an application running on Kubernetes to easily consume Kubernetes native services, which is something we'll need when we want to introduce environment-specific configuration in the next section of this lab.

1. Go back to the CodeReady Workspaces tab in your browser
1. Open `pom.xml` & find the `<properties>` section
1. Add a new property `spring-cloud.version`
    - `<spring-cloud.version>Greenwich.SR1</spring-cloud.version>`
1. Find the `<dependencyManagement>` section and add a new `<dependency>` for the [`spring-cloud`](https://spring.io/projects/spring-cloud) BOM
   ```xml
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-dependencies</artifactId>
     <version>${spring-cloud.version}</version>
     <type>pom</type>
     <scope>import</scope>
   </dependency>
   ```
1. Find the main `<dependencies>` section (outside of the `<dependencyManagement>` section) and add a new `<dependency>` for `spring-cloud-kubernetes`
   ```xml
   <dependency>
     <groupId>org.springframework.cloud</groupId>
     <artifactId>spring-cloud-starter-kubernetes-all</artifactId>
   </dependency>
   ```
1. In CodeReady Workspaces at the bottom of the workspace click on the `Terminal` tab
1. Execute `oc adm policy add-role-to-user view system:serviceaccount:$OCP_USERNAME-spring-music:default`
   > The `spring-cloud-kubernetes` project needs some permissions to be able to read from the Kubernetes API on the pod that the application is running in. This command gives the default user in the project view permissions to those APIs.

1. Re-deploy the application to OpenShift
    - In the project workspace navigation right-click the `summit-lab-spring-music (master)` project & choose `Commands -> Deploy (1) -> Deploy to Openshift`
1. Once the build finishes, go back to the browser tab which has the OpenShift console.
   > If you're quick enough you should see a rolling deployment happening.

1. Go back to the `/actuator` endpoint showing the list of actuators
1. Click on the `info` actuator link
   > At the bottom of the JSON structure you should now see a `Kubernetes` element showing information about the current kubernetes node & pod

1. Go back to the `/actuator` endpoint and click on the `health` actuator link
   > At the bottom of the JSON structure you should now see a `Kubernetes` element showing the status of the kubernetes node & pod

## Change Application Data Source
Now that our application knows how to interact with Kubernetes let's re-configure our application so that it can use an external database and not the default embedded H2 database.

### Stand up PostgreSQL database
1. In CodeReady Workspaces at the bottom of the workspace click on the `Terminal` tab, making sure the current directory is the `summit-lab-spring-music` folder
1. Execute `oc process -f misc/templates/postgresql-template.yml | oc create -f -`
   > This will create and start a PostgreSQL database within your project

1. If you go back to the OpenShift web console in your browser you should see it being created
1. The connection information/credentials are stored in a [Secrets](https://docs.openshift.com/container-platform/3.11/dev_guide/secrets.html). We need to bind these secrets to our application during deployment so that our application "picks up" the information.

### Define Resource Fragments
The [Fabric8 Maven plugin](https://maven.fabric8.io/) provides the ability for us to define [Resource Fragments](https://maven.fabric8.io/#resource-fragments) for things that we want created when the fabric8 plugin deploys an application. In OpenShift, the [Deployment Configuration](https://docs.openshift.com/container-platform/3.11/dev_guide/deployments/how_deployments_work.html) is what controls what is exposed to a pod and when new deployments should be triggered based on conditions. For this exercise we want to create a deployment configuration fragment that alters the deployment a little bit.

1. Go back to the CodeReady Workspaces tab in your browser
1. Expand the `summit-lab-spring-music/src/main` folder
1. Create a new folder called `fabric8` inside the `summit-lab-spring-music/src/main` folder
1. Inside the `summit-lab-spring-music/src/main/fabric8` folder create a new file called `deploymentconfig.yml`, using the following as it's content, making sure the indentation matches correctly
   > If the indentation isn't correct then the yaml file won't be correct and the deployment will fail.

   ```yml
   spec:
     replicas: 1
     triggers:
       - type: ConfigChange
       - type: ImageChange
         imageChangeParams:
           automatic: true
           containerNames:
             - spring-boot
             - summit-lab-spring-music
           from:
             kind: ImageStreamTag
             name: summit-lab-spring-music:1.0
     template:
       spec:
         containers:
           - env:
               - name: DB_NAME
                 valueFrom:
                   secretKeyRef:
                     name: summit-lab-spring-music-db
                     key: database-name
               - name: SPRING_DATASOURCE_USERNAME
                 valueFrom:
                   secretKeyRef:
                     name: summit-lab-spring-music-db
                     key: database-user
               - name: SPRING_DATASOURCE_PASSWORD
                 valueFrom:
                   secretKeyRef:
                     name: summit-lab-spring-music-db
                     key: database-password
               - name: SPRING_DATASOURCE_URL
                 value: 'jdbc:postgresql://summit-lab-spring-music-db/$(DB_NAME)'
   ```
1. Re-deploy the application to OpenShift
    - In the project workspace navigation right-click the `summit-lab-spring-music (master)` project & choose `Commands -> Deploy (1) -> Deploy to Openshift`
1. Once the build finishes, go back to the browser tab which has the OpenShift console.
   > If you're quick enough you should see a rolling deployment happening.

1. Go back to the `/actuator` endpoint showing the list of actuators
1. Click on the `health` actuator link
   > Notice this time in the `db` element that the database is a PostgreSQL database, not an H2 database

1. Change the end of the URL from `/actuator/health` to `/actuator/env/spring.datasource.url`
   > Notice here the value `jdbc:postgresql://summit-lab-spring-music-db/music`, which is coming from the `systemEnvironment` property source

1. Change the end of the URL from `/actuator/health` to `/actuator/env/spring.datasource.password`
   > Notice here the value has just a bunch of asterisks. The Spring Boot env actuator is smart enough to not display sensitive information

So what happened here? Using the fabric8 deployment fragment we were able to adjust the application so that when deployed, we performed a binding of the [OpenShift Secret](https://docs.openshift.com/container-platform/3.11/dev_guide/secrets.html) to environment variables that [Spring Boot recognizes for configuring a datasource](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html).

In a more real-life scenario, the steps we took to bind the database configuration to the application would have been done as part of the deployment pipeline.

## Cleanup
1. In CodeReady Workspaces at the bottom of the workspace click on the `Terminal` tab
1. Run the command `oc delete project $OCP_USERNAME-spring-music`

## Solution
If at any point you get stuck - [here is the completed solution](https://github.com/edeandrea/summit-lab-spring-music/tree/solution).
